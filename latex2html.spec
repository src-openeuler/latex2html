Name:           latex2html
Version:        2025
Release:        1
Summary:        A utility that converts LaTeX documents to web pages in HTML
License:        GPL-2.0-or-later
URL:            https://www.latex2html.org/
Source0:        https://github.com/latex2html/latex2html/archive/v%{version}/%{name}-%{version}.tar.gz
Source1:        latex2html.1
Source2:        pstoimg.1
Source3:        texexpand.1

BuildRequires:  perl-interpreter perl-generators ghostscript netpbm-progs tex(latex)
Requires:       tex(latex) tex(dvips) tex(url.sty) netpbm-progs
BuildArch:      noarch

%description
LaTeX2HTML replicates the basic structure of a LaTeX document as a set of
interconnected HTML files which can be explored using automatically generated
navigation panels. The cross-references, citations, footnotes, the table of
contents and the lists of figures and tables, are also translated into hypertext
links. Formatting information which has equivalent tags in HTML (lists, quotes,
paragraph breaks, type styles, etc.) is also converted appropriately. The
remaining heavily formatted items such as mathematical equations, pictures or
tables are converted to images which are placed automatically at the correct
positions in the final HTML document.

%package_help

%prep
%autosetup -p1

rm -f L2hos/{Dos,Mac,OS2,Win32}.pm
rm -rf cweb2html
rm -f readme.hthtml
cd ..

cp -a %{name}-%{version} %{name}-%{version}JA
cd %{name}-%{version}JA

%build
cp %{SOURCE1} %{SOURCE2} %{SOURCE3} ./

./configure	--program-prefix=%{?_program_prefix} --prefix=%{_prefix} --exec-prefix=%{_exec_prefix} \
		--bindir=%{_bindir} --sbindir=%{_sbindir} --sysconfdir=%{_sysconfdir} --datadir=%{_datadir} \
		--includedir=%{_includedir} --libdir=%{_datadir}/latex2html --libexecdir=%{_libexecdir} \
		--localstatedir=%{_localstatedir} --sharedstatedir=%{_sharedstatedir} --mandir=%{_mandir} \
		--infodir=%{_infodir} --shlibdir=%{_datadir}/latex2html --with-texpath=%{_datadir}/texmf/tex/latex/html

perl -pi -e "s,/usr/(share/)?lib,%{_datadir}," cfgcache.pm
make
cd ..

cd %{name}-%{version}JA
perl -pi -e "s,/usr/bin/dvips,/usr/bin/pdvips," cfgcache.pm
perl -pi -e "s,/usr/bin/latex,/usr/bin/platex," cfgcache.pm

./configure	--program-prefix=%{?_program_prefix} --prefix=%{_prefix} --exec-prefix=%{_exec_prefix} \
		--bindir=%{_bindir} --sbindir=%{_sbindir} --sysconfdir=%{_sysconfdir} --datadir=%{_datadir} \
		--includedir=%{_includedir} --libdir=%{_datadir}/jlatex2html --libexecdir=%{_libexecdir} \
		--localstatedir=%{_localstatedir} --sharedstatedir=%{_sharedstatedir} --mandir=%{_mandir} \
		--infodir=%{_infodir} --shlibdir=%{_datadir}/jlatex2html --with-texpath=%{_datadir}/texmf/tex/latex/html

perl -pi -e "s,/usr/(share/)?lib,%{_datadir},;
	    s,%{_datadir}/latex2html,%{_datadir}/jlatex2html," cfgcache.pm
make
perl -pi -e "s,\\\$\{dd}pstoimg,\\\$\{dd}jpstoimg, ;
	    s,\\\$\{dd}texexpand,\\\$\{dd}jtexexpand," l2hconf.pm

mv latex2html jlatex2html
mv pstoimg jpstoimg
mv texexpand jtexexpand
cd ..

%install
sed -i "s,%{_prefix},%{buildroot}%{_prefix}," cfgcache.pm
sed -i "s,%{buildroot},," l2hconf.pm

perl -pi -e "s,/.*\\\$\{dd}texexpand,%{_bindir}/texexpand,;
	    s,/.*\\\$\{dd}pstoimg,%{_bindir}/pstoimg,;
	    s,/.*\\\$\{dd}*icons,\\\$\{LATEX2HTMLDIR}/icons,;
	    s,/.*\\\$\{dd}rgb.txt,\\\$\{LATEX2HTMLDIR}/styles/rgb.txt,;
	    s,/.*\\\$\{dd}styles\\\$\{dd}crayola.txt,\\\$\{LATEX2HTMLDIR}/styles/crayola.txt," latex2html

make install
rm -rf %{buildroot}%{_datadir}/latex2html/versions/table.pl.orig \
       %{buildroot}%{_datadir}/latex2html/docs/ \
       %{buildroot}%{_datadir}/latex2html/example/
sed -i "s,/usr/local/bin/perl,/usr/bin/perl," %{buildroot}%{_datadir}/latex2html/makeseg/makeseg
sed -i "s,/usr/local/bin/perl,/usr/bin/perl," %{buildroot}%{_datadir}/latex2html/makemap
sed -i "s,###\!.*,," %{buildroot}%{_datadir}/latex2html/makemap
sed -i "s,###\!.*,," %{buildroot}%{_datadir}/latex2html/makeseg/makeseg
sed -i "s,%{buildroot},," %{buildroot}%{_bindir}/pstoimg
sed -i "s,%{buildroot},," %{buildroot}%{_bindir}/texexpand
sed -i "s,%{buildroot},," cfgcache.pm
sed -i "s,$cfg{'srcdir'}.*,$cfg{'srcdir'} = q'%{name}-%{version}';," cfgcache.pm
perl -pi -e "s,$cfg{'GS_LIB'} = q'';,$cfg{'GS_LIB'} = q'%{_datadir}/ghostscript/`ghostscript --version`';," cfgcache.pm
install -m0644 *.pm %{buildroot}%{_datadir}/latex2html
chmod +x %{buildroot}%{_datadir}/latex2html/makeseg/makeseg %{buildroot}%{_datadir}/latex2html/makemap


mkdir -p %{buildroot}%{_mandir}/man1
install -m0644 *.1 %{buildroot}%{_mandir}/man1
cd ..


cd %{name}-%{version}JA
sed -i "s,%{_prefix},%{buildroot}%{_prefix}," cfgcache.pm
perl -pi -e "s,latex2html pstoimg texexpand,jlatex2html jpstoimg jtexexpand," config/install.pl
perl -pi -e "s,/.*\\\$\{dd}texexpand,%{_bindir}/jtexexpand,;
	    s,/.*\\\$\{dd}pstoimg,%{_bindir}/jpstoimg,;
	    s,/.*\\\$\{dd}icons,\\\$\{LATEX2HTMLDIR}/icons,;
	    s,/.*\\\$\{dd}styles\\\$\{dd}rgb.txt,\\\$\{LATEX2HTMLDIR}/styles/rgb.txt,;
	    s,/.*\\\$\{dd}styles\\\$\{dd}crayola.txt,\\\$\{LATEX2HTMLDIR}/styles/crayola.txt," jlatex2html
sed -i "s,%{buildroot},," l2hconf.pm

make install
rm -rf %{buildroot}%{_datadir}/jlatex2html/versions/table.pl.orig \
       %{buildroot}%{_datadir}/jlatex2html/docs/ \
       %{buildroot}%{_datadir}/jlatex2html/example/
sed -i "s,/usr/local/bin/perl,/usr/bin/perl," %{buildroot}%{_datadir}/jlatex2html/makeseg/makeseg
sed -i "s,/usr/local/bin/perl,/usr/bin/perl," %{buildroot}%{_datadir}/jlatex2html/makemap
sed -i "s,###\!.*,," %{buildroot}%{_datadir}/jlatex2html/makemap
sed -i "s,###\!.*,," %{buildroot}%{_datadir}/jlatex2html/makeseg/makeseg
sed -i "s,%{buildroot},," %{buildroot}%{_bindir}/jpstoimg
sed -i "s,%{buildroot},," %{buildroot}%{_bindir}/jtexexpand
sed -i "s,%{buildroot},," cfgcache.pm
sed -i "s,$cfg{'srcdir'}.*,$cfg{'srcdir'} = q'%{name}-%{version}JA';," cfgcache.pm
perl -pi -e "s,$cfg{'GS_LIB'} = q'';,$cfg{'GS_LIB'} = q'%{_datadir}/ghostscript/`ghostscript --version`';," cfgcache.pm
install -m0644 *.pm %{buildroot}%{_datadir}/jlatex2html
chmod +x %{buildroot}%{_datadir}/jlatex2html/makeseg/makeseg %{buildroot}%{_datadir}/jlatex2html/makemap
cd ..

rm -f %{buildroot}%{_datadir}/texmf/tex/latex/html/url.sty

%post
[ -x %{_bindir}/texconfig-sys ] && %{_bindir}/texconfig-sys rehash 2> /dev/null || :

%postun
[ -x %{_bindir}/texconfig-sys ] && %{_bindir}/texconfig-sys rehash 2> /dev/null || :

%check
make check
cd ../%{name}-%{version}JA
make check

%files
%doc {LICENSE,LICENSE.orig,README.md,FAQ,BUGS,docs,example}
%{_bindir}/latex2html
%{_bindir}/pstoimg
%{_bindir}/texexpand
%{_bindir}/jlatex2html
%{_bindir}/jpstoimg
%{_bindir}/jtexexpand
%dir %{_datadir}/latex2html
%{_datadir}/latex2html/*
%dir %{_datadir}/texmf/tex/latex/html
%{_datadir}/texmf/tex/latex/html/*
%dir %{_datadir}/jlatex2html
%{_datadir}/jlatex2html/*

%files          help
%{_mandir}/man1/latex2html.1*
%{_mandir}/man1/texexpand.1*
%{_mandir}/man1/pstoimg.1*

%changelog
* Mon Jan 06 2025 Funda Wang <fundawang@yeah.net> - 2025-1
- update to 2025

* Thu Oct 31 2024 xu_ping <707078654@qq.com> - 2024.2-1
- Upgrade to 2024.2

* Fri Oct 13 2023 yaoxin <yao_xin001@hoperun.com> - 2023.2-1
- Upgrade to 2023.2

* Thu Feb 20 2020 lihao <lihao129@huawei.com> - 2018.3-3
- Package init
